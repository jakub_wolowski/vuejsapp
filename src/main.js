import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import './plugins/vuetify';
import App from './App.vue';
import router from './router';


Vue.use(Vuetify);

Vue.config.productionTip = false;
// eslint-disable-next-line
new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
